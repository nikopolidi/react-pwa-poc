import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import logo from '../../logo.svg';
import axios from 'axios';
import crypto from 'crypto-browserify';
import zlib from 'browserify-zlib';
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'

const P_KEY="17899578660c1b0e21a6190582b0581d1975099c64ef791a04ff2ea2a29c43a0"
const AES_ALGORITHM = 'aes-256-cbc';
const INIT_VECTOR = "14cf2f5a84ab184363da50a9cc5d7205"
const URL = "http://localhost:3000/rdata?compressed=1"
const HEADERS = `{
    "Authorization": "Bearer YOUR_TOKEN"
}
`
const Home = ({ title }) =>{
    const [data,setdata] = useState('')
    const [rdata, setrdata] = useState('')
    const [loading,setloading]= useState(false)
    const [headers, setheaders] = useState(sessionStorage.getItem('HEADERS') || HEADERS)
    const [url, seturl] = useState(sessionStorage.getItem('URL') || URL)
    const changeURL = (text)=>{
        seturl(text)
        sessionStorage.setItem('URL', text)
    }
    const changeHeaders = (text)=>{
        setheaders(text)
        sessionStorage.setItem('HEADERS', text)
    }

    const decryptAndUncompress = (encrypted) => {
        const cryptkey = crypto.createHash('sha256').update(P_KEY).digest()
        const iv = Buffer.from(INIT_VECTOR, 'hex')
        const encryptdata = Buffer.from(encrypted, 'base64')
      
        var decipher = crypto.createDecipheriv(AES_ALGORITHM, cryptkey, iv);
        
        const decrypted = Buffer.concat([
          decipher.update(encryptdata),
          decipher.final()
        ]).toString('utf8');
      
        return new Promise(resolve => (
          zlib.gunzip(Buffer.from(decrypted, 'base64'), (err, unzipped) => {
            return resolve(unzipped.toString())
          })
        ))
    }
    
    const fetchData = async ()=>{
        setloading(true)
        try{
            const headersJSON = JSON.parse(headers)
            const {data} = await axios({
                url:URL,
                method: "GET",
                headers: headersJSON
            })
            setrdata(data)
            const decrypted = await decryptAndUncompress(data)
            setdata(decrypted)
            
        }catch(err){
            console.error('error:', err);
            alert(err.message)
        }
        setloading(false)
    }

    return (
        <Grid className="App" spacing={2} container direction="column">
            <Grid className="App-header" item>
                <img src={logo} className="App-logo" alt="logo" />
                <h2>{title}</h2>
                </Grid>
                <p className="App-intro">
                    This is the {title} page.
                </p>
            <Grid>
            <p>
                <Link to="/">Home</Link>
            </p>
            </Grid>
            <Grid item style={{padding:16}}>
                <Grid container spacing={2} justify="center">
                    <Grid item sm={12} md={6}>
                        <TextField
                            value={url}
                            fullWidth
                            variant="standard"
                            onChange={e=> changeURL(e.target.value)}
                            label="Request URL"
                        />
                    </Grid>
                    <Grid item sm={12} md={6}>
                        <TextField
                            fullWidth
                            multiline
                            rows={4}
                            variant="outlined"
                            label="Headers"
                            value={headers}
                            onChange={e=> changeHeaders(e.target.value)}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Button
                            onClick={fetchData}
                            variant="contained"
                            color="primary"
                        >
                            {loading ? "Loading" : "SEND REQUEST"}
                        </Button>
                    </Grid>
                </Grid>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            id="encoded"
                            label="Encoded"
                            multiline
                            rows={4}
                            variant="filled"
                            value={rdata}
                            inputProps={{readOnly:true}}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            id="decoded"
                            label="Decoded"
                            multiline
                            rows={4}
                            variant="filled"
                            value={data}
                            inputProps={{readOnly:true}}
                        />
                    </Grid>

                </Grid>
            </Grid>
      </Grid>
    )
}

export default Home